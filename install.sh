#!/bin/bash
for file in $(ls -a | grep '^\.'); do
    if [ $file != '.git' -a $file != '.' -a $file != '..' ]; then
        cp -r $HOME/src/$USER/dotfiles/$file ~/$file
    fi
done
