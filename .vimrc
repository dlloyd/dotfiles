set tabstop=8
set shiftwidth=8
set noexpandtab
au Filetype python setl et ts=4 sw=4

autocmd BufNewFile,BufRead *.md set filetype=markdown

execute pathogen#infect()

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checkers = ['pylint', 'python']
let g:syntastic_c_checkers = ['gcc']


map <F3> :SyntasticToggle<CR>

color desert

" Open markdown style links with <F4>
map <F4> yi):!sxiv <C-R>"<CR>
