#!/bin/bash
for file in $(ls -a | grep '^\.'); do
    if [ $file != '.git' -a $file != '.' -a $file != '..' ]; then
        cp -r ~/$file $HOME/src/$USER/dotfiles/
    fi
done

unwanted='.newsbeuter/cache.db .newsbeuter/history.cmdline .newsbeuter/history.search'
rm $unwanted
